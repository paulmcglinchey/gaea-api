<?php

require('../gaeaConnect.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

if (!isset($_GET['offset'])) {
  $output['error'] = "Offset not set";
  echo json_encode($output);
  exit;
}

// get the offset parameter
$offset = $connection -> real_escape_string($_GET['offset']);

// make a request for some data
$sql = "SELECT CurrentAdminID, username, Date, Activity, Item FROM gaea_log INNER JOIN gaea_admins ON gaea_admins.AdminID = gaea_log.AdminID ORDER BY CurrentAdminID DESC LIMIT $offset,10;";

//$sql = "SELECT UrbanMetric, CombinedMetric from gaea_vehicleeconomy;";
$result = $connection -> query($sql);

// figure out how many rows we can actually access
$row = ($connection -> query("SELECT COUNT(*) FROM gaea_log;")) -> fetch_array(MYSQLI_NUM);
$maxOffset = 10 * floor(($row[0] - 1) / 10);

$output['maxOffset'] = $maxOffset;

$data = array();

if ($result -> num_rows > 0) {
  while ($row = $result -> fetch_assoc()) {
    $data[] = $row;
  }
} else {
  $data = "0 rows returned";
}

if ($data) {
  $output['data'] = $data;
} else {
  http_response_code(400);
  exit;
}

echo json_encode($output);

?>