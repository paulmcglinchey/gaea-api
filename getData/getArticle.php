<?php

require('../gaeaConnect.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

// bad request
if (!isset($_GET['articleID']) || !isset($_GET['typeContent'])) {
  http_response_code(400);
  exit;
}

$articleID = $connection -> real_escape_string($_GET['articleID']);
$typeContent = $connection -> real_escape_string($_GET['typeContent']);

$id;
if ($typeContent === "gaea_about") {
  $id = "AboutID";
} else if ($typeContent === "gaea_landing") {
  $id = "LandingID";
}

$sql = "SELECT Title, Content, URL, username FROM $typeContent 
        INNER JOIN gaea_images ON $typeContent.ImageID = gaea_images.ImageID
        INNER JOIN gaea_admins ON $typeContent.AdminID = gaea_admins.AdminID
        WHERE $id = '$articleID';";

$result = $connection -> query($sql);

$data = array();

if ($result -> num_rows > 0) {
  while ($row = $result -> fetch_assoc()) {
    $data = $row;
  }
} else {
  $data = "0 rows returned";
}

if ($data) {
  $output['data'] = $data;
} else {
  http_response_code(400);
  exit;
}

echo json_encode($output);

?>