<?php
// assoc array to store the return info
$output = array();

include('../gaeaConnect.php');
include('./../logging/logitem.php');
$connection = connectToGaea();

$expectedLength = 21;
$insertCount = 0;

// Takes raw data from the request
$file = $_FILES['file'];
$username = $_POST['username'];

if ($file['error']) {
  $output['fileError'] = $file['error'];
  echo json_encode($output);
  exit;
}

// checking if file exists
if ($file && $file['type'] == "application/vnd.ms-excel") {
  $output['fileType'] = $file['type'];
} else {
  $output['fileType'] = $file['type'];
  $output['fileError'] = "File doesn't match specified requirements";
  echo json_encode($output);
  exit;
}

$data = fopen($file['tmp_name'], "r");

if ($data) {
  $output['fileOpened'] = TRUE;
} else {
  $output['fileOpened'] = FALSE;
  exit;
}

while (! feof($data)) {
  if ($csv = fgetcsv($data)) {
    
    for ($i = 0 ; $i < $expectedLength ; $i++) {
      if (!isset($csv[$i])) {
        $output['dataError'] = "File does not follow the required specification";
        echo json_encode($output);
        exit;
      }
    }
  
    $year = $connection -> real_escape_string($csv[0]);
    $manufacturer = $connection -> real_escape_string($csv[1]);
    $model = $connection -> real_escape_string($csv[2]);
    $desc = $connection -> real_escape_string($csv[3]);
    $enginecap = $connection -> real_escape_string($csv[4]);
    $fueltype = $connection -> real_escape_string($csv[5]);
    $eurostandard = $connection -> real_escape_string($csv[6]);
    $taxband = $connection -> real_escape_string($csv[7]);
    $transmission = $connection -> real_escape_string($csv[8]);
    $transmissiontype = $connection -> real_escape_string($csv[9]);
    $urbanmetric = $connection -> real_escape_string($csv[10]);
    $extraurbanmetric = $connection -> real_escape_string($csv[11]);
    $combinedmetric = $connection -> real_escape_string($csv[12]);
    $urbanimperial = $connection -> real_escape_string($csv[13]);
    $extraurbanimperial = $connection -> real_escape_string($csv[14]);
    $combinedimperial = $connection -> real_escape_string($csv[15]);
    $fuelcost = $connection -> real_escape_string($csv[16]);
    $noiselevel = $connection -> real_escape_string($csv[17]);
    $co2 = $connection -> real_escape_string($csv[18]);
    $co = $connection -> real_escape_string($csv[19]);
    $nox = $connection -> real_escape_string($csv[20]);
  
    // checking if manufacturer exists, if not: insert entry for it
    $sqlCheck = "SELECT ManufacturerID from gaea_vehiclemanufacturer WHERE Manufacturer='$manufacturer';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehiclemanufacturer (Manufacturer) VALUES ('$manufacturer');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        echo "Error: ".$connection -> error;
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
  
    // checking if vehicle detail exists, if not: insert entry for it
    $sqlCheck = "SELECT VehicleDetailID from gaea_vehicledetail WHERE Description='$desc';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicledetail (ManufacturerID, Model, Description) VALUES
                  ((SELECT ManufacturerID from gaea_vehiclemanufacturer WHERE manufacturer='$manufacturer'),
                  '$model', '$desc');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        echo "Error: ".$connection -> error;
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle fuel type exists, if not: insert entry for it
    $sqlCheck = "SELECT FuelTypeID from gaea_vehiclefueltype WHERE FuelType='$fueltype';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehiclefueltype (FuelType) VALUES ('$fueltype');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        echo "Error: ".$connection -> error;
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle engine exists, if not: insert entry for it
    $sqlCheck = "SELECT EngineID from gaea_vehicleengine WHERE EngineSize='$enginecap';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicleengine (FuelTypeID, EngineSize) VALUES
                  ((SELECT FuelTypeID from gaea_vehiclefueltype WHERE FuelType='$fueltype'),
                  '$enginecap');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle year exists, if not: insert entry for it
    $sqlCheck = "SELECT YearID from gaea_vehicleyear WHERE Year='$year';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicleyear (Year) VALUES ('$year');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle transmission type exists, if not: insert entry for it
    $sqlCheck = "SELECT TransmissionTypeID from gaea_vehicletransmissiontype WHERE TransmissionType='$transmissiontype';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicletransmissiontype (TransmissionType) VALUES ('$transmissiontype');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle transmission exists, if not: insert entry for it
    $sqlCheck = "SELECT TransmissionID from gaea_vehicletransmission WHERE Transmission='$transmission';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicletransmission (TransmissionTypeID, Transmission) VALUES
                  ((SELECT TransmissionTypeID from gaea_vehicletransmissiontype WHERE TransmissionType='$transmissiontype'),
                  '$transmission');";
                  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle economy data exists, if not: insert entry for it
    $sqlCheck = "SELECT EconomyID from gaea_vehicleeconomy WHERE UrbanMetric='$urbanmetric' AND FuelCost12000Miles='$fuelcost';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicleeconomy (UrbanMetric, ExtraUrbanMetric, CombinedMetric, UrbanImperial, ExtraUrbanImperial, CombinedImperial, FuelCost12000Miles) VALUES
                  ('$urbanmetric', '$extraurbanmetric', '$combinedmetric', '$urbanimperial', '$extraurbanimperial', '$combinedimperial', '$fuelcost');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle emissions data exists, if not: insert entry for it
    $sqlCheck = "SELECT EmissionsID from gaea_vehicleemissions WHERE NoiseLevel='$noiselevel' AND NOX='$nox';";
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicleemissions (NoiseLevel, CO2, CO, NOX) VALUES
                  ('$noiselevel', '$co2', '$co', '$nox');";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }
    
    // checking if vehicle exists already, if not: insert entry for it
    $sqlCheck = "SELECT VehicleID from gaea_vehicle WHERE 
                VehicleDetailID=(SELECT VehicleDetailID from gaea_vehicledetail WHERE Description='$desc') AND
                EngineID=(SELECT EngineID from gaea_vehicleengine WHERE EngineSize='$enginecap')";
  
    $result = $connection -> query($sqlCheck);
    if (!($result -> fetch_assoc())) {
      $sqlEntry = "INSERT INTO gaea_vehicle (VehicleDetailID, YearID, TransmissionID, EngineID, TaxBand, EuroStandard, EconomyID, EmissionsID, AdminID) VALUES
                  ((SELECT VehicleDetailID from gaea_vehicledetail WHERE Description='$desc'),
                  (SELECT YearID from gaea_vehicleyear WHERE Year='$year'),
                  (SELECT TransmissionID from gaea_vehicletransmission WHERE Transmission='$transmission'),
                  (SELECT EngineID from gaea_vehicleengine WHERE EngineSize='$enginecap'),
                  '$taxband',
                  '$eurostandard',
                  (SELECT EconomyID from gaea_vehicleeconomy WHERE UrbanMetric='$urbanmetric' AND FuelCost12000Miles='$fuelcost'),
                  (SELECT EmissionsID from gaea_vehicleemissions WHERE NoiseLevel='$noiselevel' AND NOX='$nox'),
                  (SELECT AdminID from gaea_admins WHERE username='$username'));";
  
      if (!$connection -> query($sqlEntry) === TRUE) {
        $output['dataError'] = $connection -> error;
        echo json_encode($output);
        exit;
      }
    }

    $insertCount = $insertCount + 1;
  }
}

logItem($connection, $username, "inserted", "$insertCount row(s) of data");

$output['message'] = "Data uploaded!";
echo json_encode($output);

?>