<?php

require('../logging/logitem.php');
require('../gaeaConnect.php');
$connection = connectToGaea();

// no connection to the database
if (!$connection) {
  $output['error'] = $connection -> error;
  echo json_encode($output);
  exit;
}

// bad request
if (!isset($_GET['articleID']) || !isset($_GET['username']) || !isset($_GET['typeContent'])) {
  http_response_code(400);
  exit;
}

$articleID = $connection -> real_escape_string($_GET['articleID']);
$username = $connection -> real_escape_string($_GET['username']);
$typeContent = $connection -> real_escape_string($_GET['typeContent']);

$id;

if ($typeContent === "gaea_about") {
  $id = "AboutID";
} else if ($typeContent === "gaea_landing") {
  $id = "LandingID";
}

$sql = "DELETE FROM $typeContent WHERE $id='$articleID';";
$result = $connection -> query($sql);

if ($result === TRUE) {
  $output['message'] = "deleted article $articleID from $typeContent";
  logItem($connection, $username, "deleted", "article $articleID from $typeContent");
  http_response_code(200);
} else {
  $output['error'] = $connection -> error;
  http_response_code(400);
}

echo json_encode($output);

?>