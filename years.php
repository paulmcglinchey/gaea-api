<?php

include('gaeaConnect.php');

$connection = connectToGaea();

$data = $connection -> query("SELECT * FROM gaea_vehicleyear");

if (!$data) {
    echo $connection->error;
}

while ($row = $data->fetch_object()) {
    $years_arr[] = $row;
}

echo json_encode($years_arr);

?>