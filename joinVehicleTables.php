<?php

function joinVehicleTables($connection, $column1, $column2) {
  $sql = "SELECT DISTINCT $column1, $column2 FROM gaea_vehicle
          INNER JOIN gaea_vehicledetail ON gaea_vehicle.VehicleDetailID = gaea_vehicledetail.VehicleDetailID
          INNER JOIN gaea_vehiclemanufacturer ON gaea_vehicledetail.ManufacturerID = gaea_vehiclemanufacturer.ManufacturerID
          INNER JOIN gaea_vehicleengine ON gaea_vehicle.EngineID = gaea_vehicleengine.EngineID
          INNER JOIN gaea_vehicletransmission ON gaea_vehicle.TransmissionID = gaea_vehicletransmission.TransmissionID
          INNER JOIN gaea_vehicletransmissiontype ON gaea_vehicletransmission.TransmissionTypeID = gaea_vehicletransmissiontype.TransmissionTypeID
          INNER JOIN gaea_vehicleyear ON gaea_vehicle.YearID = gaea_vehicleyear.YearID
          INNER JOIN gaea_vehiclefueltype ON gaea_vehicleengine.FuelTypeID = gaea_vehiclefueltype.FuelTypeID
          INNER JOIN gaea_vehicleemissions ON gaea_vehicle.EmissionsID = gaea_vehicleemissions.EmissionsID
          INNER JOIN gaea_vehicleeconomy ON gaea_vehicle.EconomyID = gaea_vehicleeconomy.EconomyID;";

  return $sql;
}

?>