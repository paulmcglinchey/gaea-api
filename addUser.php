<?php

include('gaeaConnect.php');
include('./logging/logitem.php');
$connection = connectToGaea();

// Takes raw data from the request
$json = file_get_contents('php://input');

// Converts it into a PHP object
$data = json_decode($json, true);

if (!isset($data['username']) || !isset($data['password']) || !isset($data['admin'])) {
  http_response_code(400);
  exit;
}

$username = $connection -> real_escape_string($data['username']);
$password = hash("sha256", $data['password'], false);
$admin = $connection -> real_escape_string($data['admin']);

$sql = "INSERT INTO gaea_admins (username, password) VALUES ('$username', '$password');";

if ($connection -> query($sql) === TRUE) {
  http_response_code(200);
  logItem($connection, $admin, "added", "new user: $username");
  echo "$username successfully added to users";
  exit;
} else {
  http_response_code(400);
  echo "Error with: $sql<br> ".$connection->error;
  exit;
}

?>